#-------------------------------------------------
#
# Project created by QtCreator 2017-09-10T14:10:58
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_yksikkotestaustest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_yksikkotestaustest.cpp \
            ../morottaja.cc
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
        ../morottaja.hh

INCLUDEPATH += ../
DEPENDPATH  += ../



