#include <QString>
#include <QtTest>

#include "morottaja.hh"
class YksikkotestausTest : public QObject
{
    Q_OBJECT

public:
    YksikkotestausTest();

private Q_SLOTS:
    void manseTest();
    void nimiTestaus();
    void montakoTestaus();
};

YksikkotestausTest::YksikkotestausTest()
{
}

void YksikkotestausTest::manseTest()
{
    //Testaus, jolla testataan mansen onnistunutta morottamista
    Morottaja m; // Luodaan Morottajaluokkaan olio m
    std::string expected = "Morjesta Manse!"; //Syötetään oletettu merkkijono mikä halutaan
    std::string result = m.morjesta();  //Kutsutaan morjesta-funkiota oliolle m
    QCOMPARE(expected, result); // Verrataan, että merkkjonot täsmäävät

}

void YksikkotestausTest::nimiTestaus()
{
    //Testaus jolla testataan muiden nimisten kaupunkien morottamista
    Morottaja m; // Luodaan Morottajaluokkaan olio m
    std::string nimi = "Helsinki"; //Syöttää kaupungin nimeksi Helsinki
    std::string expected = "Morjesta, " + nimi + "!"; // Syötetään oletettu merkkijono muuttujaan expected
    std::string result = m.morjesta(nimi); // //Kutsutaan morjesta-funkiota oliolle m parametrillä nimi
    QCOMPARE(expected, result); //Verrataan, että merkkijonot täsmäävät

}

void YksikkotestausTest::montakoTestaus()
{
    //Testataan, että morotetut aina lisääntyvät
    //funktiota morjesta kutsuttaessa.
    unsigned int i = 0; //morjesta-funktion kutsumäärä
    Morottaja m;
    while (i <= 4) // Kutsutaan morjesta-funktiota 5 kertaa
    {
        i++;
        m.morjesta("Helsinki"); //Syötetään morottajalle kaupungin nimeksi Helsinki. Nimi voisi olla tässä mikä tahansa.
    }
    QCOMPARE(i, m.montakoMorotettu()); //Verrataan, että morotettujen määrä täsmää funktion morjesta kutsumäärien kanssa

}

QTEST_APPLESS_MAIN(YksikkotestausTest)

#include "tst_yksikkotestaustest.moc"
