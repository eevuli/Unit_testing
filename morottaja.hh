#ifndef MOROTTAJA_HH
#define MOROTTAJA_HH

#include <string>
/**
 * @brief Morottaja morjestaa Mansea ja Manselaisia
 */
class Morottaja
{
public:
    /**
     * @brief Morottaja oletusrakentaja rakentaa morottajan
     *
     */
    Morottaja();
    /**
     * @brief morjesta Morjestaa mansea, eli palauttaa tamperelaisen version "Hello World!":stä
     * @return "Morjesta Manse!"
     */
    std::string morjesta();
    /**
     * @brief morjesta Morjestaa tuttua.
     * @param name ketä morotetaan
     * @post Lisää morotettujen määrää yhdellä
     * @return "Morjesta, <nimi>!"
     */
    std::string morjesta(std::string nimi);
    /**
     * @brief montakoMorotettu kerto morotettujen määrän.
     * @return morotettujen määrä kokonaislukuna
     */
    unsigned montakoMorotettu();

private:

    unsigned morotetut;
};

#endif // MOROTTAJA_HH
