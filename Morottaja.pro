TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cc \
    morottaja.cc

HEADERS += \
    morottaja.hh
